<?php

class views_facets_plugin_style_facet_summary extends views_plugin_style_summary {

  function render() {
    $rows = array();

    if ($this->display->handler->get_option('facet_all_link')) {
      // Add the wildcard row:
      $argument_name = $this->view->build_info['summary_level'];
      $facet_argument = &$this->view->argument[$argument_name];
      $row = new stdClass();
      $row->{$facet_argument->base_alias} = $facet_argument->options['facet_wildcard'];
      $row->{$facet_argument->name_alias} = t('All');
      $row->{$facet_argument->count_alias} = NULL;
      $rows[] = $row;
    }

    // Now just add the other rows:

    foreach ($this->view->result as $row) {
      // @todo: Include separator as an option.
      $rows[] = $row;
    }
    return theme($this->theme_functions(), $this->view, $this->options, $rows);
  }
}
