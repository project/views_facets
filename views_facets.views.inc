<?php


/**
 * Implementation of hook_views_plugins().
 */
function views_facets_views_plugins() {
  $plugins = array(
    'display' => array(
      'facet_attachment' => array(
        'title' => t('Views facet'),
        'help' => t('TODO: write help here.'),
        'handler' => 'views_facets_plugin_display_facet_attachment',
        'theme' => 'views_view',
        'use ajax' => TRUE,
        'help topic' => 'display-facet-attachment',
        'parent' => 'block_attachment',
        'uses hook block' => TRUE,
      ),
    ),
    'style' => array(
      'facet_summary' => array(
        'title' => t('Facet List'),
        'help' => t('Displays the default summary as a facet list.'),
        'handler' => 'views_facets_plugin_style_facet_summary',
        'theme' => 'views_facets_view_facet_summary',
        'type' => 'summary', // only shows up as a summary style
        'uses options' => TRUE,
        'help topic' => 'style-summary',
        'parent' => 'default_summary',
      ),
      'facet_summary_date' => array(
        'title' => t('Facet List - Date specific'),
        'help' => t('Displays the default summary as a facet list.'),
        'handler' => 'views_facets_plugin_style_facet_summary',
        'theme' => 'views_facets_view_facet_summary_date',
        'type' => 'summary', // only shows up as a summary style
        'uses options' => TRUE,
        'help topic' => 'style-summary',
        'parent' => 'default_summary',
      ),
    )
  );

  return $plugins;
}

/**
 * Implementation of hook_views_data_alter()
 */
function views_facets_views_data_alter(&$data) {



  $faceted_handlers = views_facets_get_faceted_handlers();

  // Copy over the defined arguments:
//dsm($data);
  // Loop over all the tables:
  foreach ($data as $table_name => $table) {
    // And all the fields
    foreach ($table as $field_name => $field) {
      // Does this field have an argument:
      if (isset($data[$table_name][$field_name]['argument'])) {
        // Loop over the handlers we've defined:
        foreach ($faceted_handlers as $faceted_handler => $faceted_handler_info) {
          $original_handler = $faceted_handler_info['based_on'];
          // Is this handler one of the ones we've extended:
          if ($data[$table_name][$field_name]['argument']['handler'] == $original_handler) {
            // Copy over the argument:
            $group = isset($data[$table_name][$field_name]['group']) ? $data[$table_name][$field_name]['group'] : $data[$table_name]['table']['group'];
            $title = isset($data[$table_name][$field_name]['title']) ? $data[$table_name][$field_name]['title'] : $data[$table_name]['table']['title'];
            $data[$table_name][$field_name . '_facet'] = array(
              'group' => $group . ' - Faceted',
              'title' => $title . ' - Faceted',
              'help' => $data[$table_name][$field_name]['help'],
              'argument' => $data[$table_name][$field_name]['argument'],
            );
            $data[$table_name][$field_name . '_facet']['argument']['handler'] = $faceted_handler;
            if (!isset($data[$table_name][$field_name . '_facet']['argument']['field'])) {
              $data[$table_name][$field_name . '_facet']['argument']['field'] = $field_name;
            }
          }
        }
      }
    }
  }
}

/**
 * Implementation of hook_views_handlers().
 *
 * Register all our custom handlers with views.
 */
function views_facets_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_facets') . '/views/handlers',
    ),
    'handlers' => array(
      'views_facets_handler_argument_term_node_tid_faceted' => array(
        'parent' => 'views_handler_argument_term_node_tid',
      ),
      'views_facets_handler_argument_date_faceted' => array(
        'parent' => 'views_handler_argument_node_created_year_month',
      ),
    ),

  );
}

/**
 * Implementation of hook_views_facets_get_faceted_handlers().
 */
function views_facets_views_facets_get_faceted_handlers() {
  $facets['views_facets_handler_argument_term_node_tid_faceted'] = array(
    'based_on' => 'views_handler_argument_term_node_tid',
  );

  $facets['views_facets_handler_argument_date_faceted'] = array(
    'based_on' => 'views_handler_argument_node_created_year_month',
  );

  return $facets;
}