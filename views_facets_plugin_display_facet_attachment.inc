<?php



class views_facets_plugin_display_facet_attachment extends views_attachment_block_plugin_display_block_attachment {
  function option_definition() {
    $options = parent::option_definition();

    // Set some options:
    $options['inherit_arguments']['default'] = TRUE;
    $options['inherit_exposed_filters']['default'] = TRUE;
    $options['inherit_pager']['default'] = TRUE;
    $options['render_pager']['default'] = FALSE;

    $options['facet_all_link'] = array('default' => TRUE);
    $options['facet_clicked_behavior'] = array('default' => FALSE);


    return $options;
  }

  /**
   * Provide the summary for attachment options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);

    unset($options['inherit_arguments']);
    unset($options['inherit_pager']);
    unset($options['render_pager']);

    $options['facet_all_link'] = array(
      'category' => 'attachment',
      'title' => t('Display all link'),
      'value' => $this->get_option('facet_all_link') ? t('Yes') : t('No'),
    );

    $options['facet_clicked_behavior'] = array(
      'category' => 'attachment',
      'title' => t('Facet selected behavior'),
      'value' => $this->get_option('facet_clicked_behavior') ? t('Collapse') : t("Don't collapse"),
    );

  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'facet_all_link':
        $form['#title'] .= t('Display all link');
        $form['facet_all_link'] = array(
          '#type' => 'checkbox',
          '#title' => t('Display all link'),
          '#description' => t("Should an 'all' link be added to the list of facet options?"),
          '#default_value' => $this->get_option('facet_all_link'),
        );
        break;
      case 'facet_clicked_behavior':
        $form['#title'] .= t('Facet selected behavior');
        $form['facet_clicked_behavior'] = array(
          '#type' => 'checkbox',
          '#title' => t('Collapse'),
          '#description' => t("Should the list of facet options collapse when one is selected?"),
          '#default_value' => $this->get_option('facet_clicked_behavior'),
        );
        break;
    }
  }

    /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'facet_all_link':
      case 'facet_clicked_behavior':
        $this->set_option($form_state['section'], $form_state['values'][$form_state['section']]);
        break;
    }
  }
}
