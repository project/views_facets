<?php

class views_facets_handler_argument_date_faceted extends views_handler_argument_node_created_year_month {
  /**
   * This method does what views does, but with a twist.
   *
   * Crutially we keep processing arguments, even when one fails validation.
   * This allows the faceted counts to work properly.
   */
  function process_remaining_arguments(&$view) {
    // build arguments.
    $position = -1;

    // Create a title for use in the breadcrumb trail.
    $title = $view->display_handler->get_option('title');

    //$this->build_info['breadcrumb'] = array();
    $breadcrumb_args = array();
    $substitutions = array();

    $status = TRUE;

    // Iterate through each argument and process.
    foreach ($view->argument as $id => $arg) {
      $position++;
      // Skip the arguments already processed:
      if ($position <= $this->position) {
        continue;
      }
      $argument = &$view->argument[$id];

      if ($argument->broken()) {
        continue;
      }

      $argument->set_relationship();

      $arg = isset($view->args[$position]) ? $view->args[$position] : NULL;
      $argument->position = $position;

      if (isset($arg) || $argument->has_default_argument()) {
        if (!isset($arg)) {
          $arg = $argument->get_default_argument();
          // make sure default args get put back.
          if (isset($arg)) {
            $this->args[$position] = $arg;
          }
        }

        // Set the argument, which will also validate that the argument can be set.
        if (!$argument->set_argument($arg)) {
          $status = $argument->validate_fail($arg);
          continue;
        }

        if ($argument->is_wildcard()) {
          $arg_title = $argument->wildcard_title();
        }
        else {
          $arg_title = $argument->get_title();
          $argument->query();
        }

      }

      // Be safe with references and loops:
      unset($argument);
    }
  }

  function summary_basics($count_field = TRUE) {
    $this->process_remaining_arguments($this->view);

    parent::summary_basics($count_fields);
  }

  function option_definition() {
    $options = parent::option_definition();


    $options['style_plugin'] = array('default' => 'facet_summary_date');
    $options['default_action']['default'] = 'summary asc';
    $options['validate_type']['default'] = 'php';
    $options['validate_fail']['default'] = 'summary asc';

    $options['facet_wildcard'] = array('default' => $options['wildcard']['default']);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Force some settings for Facets:
    $form['wildcard'] = array(
      '#type' => 'value',
      '#value' => '',
    );

    $form['validate_type']['#options'] = array('php' => t('Always fail'));

    $form['validate_argument_php'] = array(
      '#type' => 'value',
      '#value' => 'return FALSE;',
    );


    // Insert just after the wildcard bit:
    $new_form = array();
    foreach ($form as $key => $value) {
      $new_form[$key] = $value;

      if ($key == 'wildcard') {
        $new_form['facet_wildcard'] = array(
          '#type' => 'textfield',
          '#title' => t('Facet wildcard'),
          '#description' => t('The wildcard to set when removing this facet. This should match the wildcard on the matching argument on the attached display.'),
          '#default_value' => $this->options['facet_wildcard'],
          '#size' => 20,
        );
      }
    }

    $form = $new_form;
  }

  /**
   * Override default_actions() to remove actions that don't
   * make sense for a null argument.
   */
  function default_actions($which = NULL) {
    if ($which) {
      if (in_array($which, array('summary asc', 'summary desc'))) {
        return parent::default_actions($which);
      }
      return;
    }
    $actions = parent::default_actions();
    unset($actions['ignore']);
    unset($actions['not found']);
    unset($actions['empty']);
    unset($actions['default']);
    return $actions;
  }

  /**
   * Provide a link to the next level of the view
   */
  function summary_name($data) {
    $created = $data->{$this->name_alias};
    if (!is_numeric($created)) {
      return check_plain($created);
    }
    return format_date(strtotime($created . "15"), 'custom', $this->format, 0);
  }

  /**
   * Provide a link to the next level of the view
   */
  function summary_year($data) {
    $created = $data->{$this->name_alias};
    if (!is_numeric($created)) {
      return check_plain($created);
    }
    return format_date(strtotime($created . "15"), 'custom', 'Y', 0);
  }

  /**
   * Provide a link to the next level of the view
   */
  function summary_month($data) {
    $created = $data->{$this->name_alias};
    if (!is_numeric($created)) {
      return check_plain($created);
    }
    return format_date(strtotime($created . "15"), 'custom', 'F', 0);
  }

  /**
   * Provide the argument to use to link from the summary to the next level;
   * this will be called once per row of a summary, and used as part of
   * $view->get_url().
   *
   * @param $data
   *   The query results for the row.
   */
  function summary_argument($data) {
    return $data->{$this->base_alias} == t('All') ? $this->options['facet_wildcard'] : $data->{$this->base_alias};
  }
}
