<?php

class views_facets_handler_argument_term_node_tid_faceted extends views_handler_argument_term_node_tid {

  /**
   * This method does what views does, but with a twist.
   *
   * Crutially we keep processing arguments, even when one fails validation.
   * This allows the faceted counts to work properly.
   */
  function process_remaining_arguments(&$view) {
    // build arguments.
    $position = -1;

    // Create a title for use in the breadcrumb trail.
    $title = $view->display_handler->get_option('title');

    //$this->build_info['breadcrumb'] = array();
    $breadcrumb_args = array();
    $substitutions = array();

    $status = TRUE;

    // Iterate through each argument and process.
    foreach ($view->argument as $id => $arg) {
      $position++;
      // Skip the arguments already processed:
      if ($position <= $this->position) {
        continue;
      }
      $argument = &$view->argument[$id];

      if ($argument->broken()) {
        continue;
      }

      $argument->set_relationship();

      $arg = isset($view->args[$position]) ? $view->args[$position] : NULL;
      $argument->position = $position;

      if (isset($arg) || $argument->has_default_argument()) {
        if (!isset($arg)) {
          $arg = $argument->get_default_argument();
          // make sure default args get put back.
          if (isset($arg)) {
            $this->args[$position] = $arg;
          }
        }

        // Set the argument, which will also validate that the argument can be set.
        if (!$argument->set_argument($arg)) {
          $status = $argument->validate_fail($arg);
          continue;
        }

        if ($argument->is_wildcard()) {
          $arg_title = $argument->wildcard_title();
        }
        else {
          $arg_title = $argument->get_title();
          $argument->query();
        }

      }

      // Be safe with references and loops:
      unset($argument);
    }
  }

  function summary_basics($count_field = TRUE) {
    $this->process_remaining_arguments($this->view);

    // Filter by vid, if selected:
    $vids = array_filter($this->options['facet_vid']);
    if (count($vids)) {
      $this->query->add_where(0, "$this->name_table_alias.vid IN (%s)", implode(",", $vids));
    }

    // Add sorting if it's been requested.
    if ($this->options['add_sorting'] == 'add_sort') {
      $this->view->_build('sort');
    }


    parent::summary_basics($count_fields);

  }

  function option_definition() {
    $options = parent::option_definition();


    $options['style_plugin'] = array('default' => 'facet_summary');
    $options['default_action']['default'] = 'summary asc';
    $options['validate_type']['default'] = 'php';
    $options['validate_fail']['default'] = 'summary asc';

    $options['facet_wildcard'] = array('default' => $options['wildcard']['default']);
    $options['facet_vid'] = array('default' => array());
    $options['add_sorting'] = array('default' => 'no_sort');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Force some settings for Facets:
    $form['wildcard'] = array(
      '#type' => 'value',
      '#value' => '',
    );

    $form['validate_type']['#options'] = array('php' => t('Always fail'));

    $form['validate_argument_php'] = array(
      '#type' => 'value',
      '#value' => 'return FALSE;',
    );


    // Insert just after the wildcard bit:
    $new_form = array();
    foreach ($form as $key => $value) {
      $new_form[$key] = $value;

      if ($key == 'wildcard') {
        $new_form['facet_wildcard'] = array(
          '#type' => 'textfield',
          '#title' => t('Facet wildcard'),
          '#description' => t('The wildcard to set when removing this facet. This should match the wildcard on the matching argument on the attached display.'),
          '#default_value' => $this->options['facet_wildcard'],
          '#size' => 20,
        );
      }
    }

    $form = $new_form;

    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabularies as $voc) {
      $options[$voc->vid] = check_plain($voc->name);
    }

    $form['facet_vid'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Restrict to vocabulary'),
      '#options' => $options,
      '#description' => t('Select which vocabularies to restrict the facet list to.'),
      '#default_value' => !empty($this->options['facet_vid']) ? $this->options['facet_vid'] : array(),
    );

    $form['add_sorting'] = array(
      '#type' => 'select',
      '#title' => t('Sorting'),
      '#options' => array(
        'add_sort' => t('Add sorting from the view'),
        'no_sort' => t('Don\'t add sorting from the view'),
      ),
      '#default_value' => $this->options['add_sorting'],
    );

  }

  /**
   * Override default_actions() to remove actions that don't
   * make sense for a null argument.
   */
  function default_actions($which = NULL) {
    if ($which) {
      if (in_array($which, array('summary asc', 'summary desc'))) {
        return parent::default_actions($which);
      }
      return;
    }
    $actions = parent::default_actions();
    unset($actions['ignore']);
    unset($actions['not found']);
    unset($actions['empty']);
    unset($actions['default']);
    return $actions;
  }

}
