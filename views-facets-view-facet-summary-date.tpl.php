<?php
/**
 * @file views-view-summary.tpl.php
 * Default simple view template to display a list of summary lines
 *
 * @ingroup views_templates
 */
?>
<div class="item-list">
  <ul class="views-summary">
  <?php foreach ($rows as $id => $row): ?>
  <?php if (is_array($row)): ?>
    <li><?php print $id; ?>
    <ul class="views-summary">
      <?php foreach ($row as $id => $inner_row): ?>
        <li><a href="<?php print $inner_row->url; ?>"<?php print !empty($classes[$id]) ? ' class="'. $classes[$id] .'"' : ''; ?>><?php print $inner_row->link; ?></a>
          <?php if (!empty($inner_row->count)): ?>
            (<?php print $inner_row->count?>)
          <?php endif; ?>
        </li>
      <?php endforeach; ?>
    </ul>
    </li>
  <?php else: ?>
    <li><a href="<?php print $row->url; ?>"<?php print !empty($classes[$id]) ? ' class="'. $classes[$id] .'"' : ''; ?>><?php print $row->link; ?></a>
      <?php if (!empty($row->count)): ?>
        (<?php print $row->count?>)
      <?php endif; ?>
    </li>
  <?php endif; ?>
  <?php endforeach; ?>
  </ul>
</div>
